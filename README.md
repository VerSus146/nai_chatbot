# NAI_ChatBot


## Autorzy
Krzysztof Windorpski - s18562 <br>
Krystian Dąbrowski - s18550

## Milestone 1

### Załóż repozytorium wraz z systemem do zarządzania zadaniami (np. GitLab i Issue)

Git - Gitlab <br>
Trello - https://trello.com/invite/b/Vh9Imvjs/1133fe8525f41e8dc67f92fe96c51902/chat-bot

### Zaplanuj zadania przypisując je do poszczególnych członków N-osobowego zespołu.

Zadania znajdziemy w Issues

### Przygotuj listę hobby człownków zespołu (dodaj do read.me projektu); każdy członek zespołu musi mieć unikatowe zainteresowania

Krzychu - Jedzenie <br>
Krystian - Gry wideo

### Przeprowadź research zagadnienia w oparciu o źródła w ilości nie mniejszej niż 5 x N

Tensorflow - https://en.wikipedia.org/wiki/TensorFlow <br>
Tensorflow Machine Learning - https://towardsdatascience.com/deep-learning-with-tensorflow-part-1-b19ce7803428 <br>
Tensorflow Neural Network - https://playground.tensorflow.org/#activation=tanh&batchSize=10&dataset=circle&regDataset=reg-plane&learningRate=0.03&regularizationRate=0&noise=0&networkShape=4,2&seed=0.25950&showTestData=false&discretize=false&percTrainData=50&x=true&y=true&xTimesY=false&xSquared=false&ySquared=false&cosX=false&sinX=false&cosY=false&sinY=false&collectStats=false&problem=classification&initZero=false&hideText=false <br>
NLP - https://en.wikipedia.org/wiki/Natural_language_processing#:~:text=Natural%20language%20processing%20(NLP)%20is,amounts%20of%20natural%20language%20data. <br>
NLP Tensorflow - https://www.youtube.com/watch?v=fNxaJsNG3-s&list=PLQY2H8rRoyvzDbLUZkbudP-MFQZwNmU4S&ab_channel=TensorFlow <br>
NLP Tokenization - https://www.analyticsvidhya.com/blog/2020/05/what-is-tokenization-nlp/ <br>
NLP Stemmization - https://medium.com/@tusharsri/nlp-a-quick-guide-to-stemming-60f1ca5db49e <br>
NLP Lemmatization - https://towardsdatascience.com/lemmatization-in-natural-language-processing-nlp-and-machine-learning-a4416f69a7b6 <br>
NLP Sequencing - https://www.geeksforgeeks.org/nlp-sequencing/ <br>
Python Pandas - https://en.wikipedia.org/wiki/Pandas_(software) <br>
Python Numpy - https://en.wikipedia.org/wiki/NumPy <br>
Kaggle Movie Dialog Corpus - https://www.kaggle.com/Cornell-University/movie-dialog-corpus 

### Sporządź podsumowanie analizy źródeł; udostępnij repozytorium

Do naszego projektu wykorzystamy <b>Tensorflow</b>, gdyż jesteśmy najlepiej obeznani z tym narzędziem i w internecie znajdziemy najwięcej informacji na temat NLP z wykorzystaniem tej technologii. <br> <br>

Pierwszym krokiem będzie uzyskanie odpowiednich danych na temat naszych hobby z różnych źródeł, takich jak fora, strony wikipedia, strony tematyczne i literatura ( o ile dostępne ). <br>
Chcielibyśmy przeparsować nasze dane podobnie do zadań z "Movie Qoutes Dataset" których jest dużo w internecie, ponieważ jest to jedno z podstawowych zadań nauki NLP. Postanowiliśmy więc na początku przeanalizować dostępne rozwiązania ze strony Kaggle i nauczyć się na podstawie dostępnych tam rozwiązań podejścia do zadań NLP.<br><br>

Tekst zanim podamy naszemu modelowi do nauki musimy podać paru obróbkom, takim jak Stemmizacja/Lemmatizacja, Tokenizacja i Sekwencjowanie, żeby uzyskać jak najlepszą formę inputu dla naszej sieci neuronowej. Podzielenie tekstu w taki sposób pozwoli naszemu modelowi nauczyć się rozróżniania naszego inputu na zasadzie "rozpoznawania" wprowadzonych przez nas wyrazów, a nie całego zdania, co pozwoli na zmniejszenie potrzebnej ilości danych wejściowych, a zarazem zwiększy ( teoretycznie ) accuracy przewidywań naszego modelu. <br><br>

Do nauki i wytworzenia kodu wykorzystamy uzyskane podczas wykładów materiały, oraz wyżej wskazane źródła oraz technologie.<br><br>

Kanał Youtube "TensorFlow" jest oficjalnym kanałem zawierających wiele filmów będących wprowadzeniem do zagadnień Machine Learningu. Wykorzystaliśmy to do nauki i pokierowania nas w odpowiednim kierunku do znalezienia odpowiednich źródeł w internecie. <br><br>

![Trello.png](./Trello.png)<br>
Więcej tasków przyjdzie gdy sprawdzimy temat i dowiemy się czego musimy się dokładnie chwycić.
