import requests
import json

apiKeyString = '1afe6f6ef6b449908cda8809bf889d67'
apiBaseUrl = 'https://api.spoonacular.com/recipes/'
apiComplexSearch = 'complexSearch'
apiRandom = 'random'

diets = ['gluten free', 'ketogenic', 'vegetarian', 'lacto-vegetarian', 'ovo-vegetarian', 'vegan', 'pescatarian',
         'paleo', 'primal']
meal_types = ['main course', 'side dish', 'dessert', 'appetizer', 'salad', 'bread', 'breakfast', 'soup', 'beverage',
              'sauce', 'marinade', 'fingerfood', 'snack', 'drink']
cuisines = ['african', 'american', 'british', 'chinese', 'european', 'eastern european', 'french', 'german', 'greek',
            'indian', 'italian', 'japanese', 'korean', 'mediterranean', 'spanish', 'thai', 'vietnamese']


def get_specific_recipe(filters):
    """
        see for available options https://spoonacular.com/food-api/docs#Get-Random-Recipes
    """
    query = apiBaseUrl + apiRandom
    tags = ','.join(filters)
    return requests.get(query, {'apiKey': apiKeyString, 'tags': tags}).json()


def get_specific_recipe_url(filters):
    return get_specific_recipe(filters)['recipes'][0]['spoonacularSourceUrl']


def food_like_patterns():
    patterns = []
    food = [
        'What food do you like?',
        'What do you recommend?',
        'What can I eat?',
    ]
    cuis = ','.join(cuisines)
    return {
        'tag': 'food-like',
        'patterns': food,
        'responses': [
            'Well it depends from the day or the weather but I like thai and mediterranean, what do you like ?',
            'I recommend to try korean or thai',
            f'You can eat many of different food like {cuis}'
        ],
        'context_set': 'food-recipe',
    }


def food_diets_patterns():
    diet = ','.join(diets)
    return {
        'tag': 'food-diet',
        'patterns': [
            'What diets',
            'What diet types',
            'Diet types',
            'Show diet types'
        ],
        'responses': [
            f'Here are some diets: {diet}',
            f'For which diet would you like a recipe: {diet}?'
            f'I know recipes for this diets: {diet}'
        ],
        'context_set': 'food-recipe',
    }


def food_cuisine_patterns():
    cuisine = ','.join(cuisines)
    return {
        'tag': 'food-cuisine',
        'patterns': [
            'What cuisine',
            'Regional food',
            'Cuisine types',
            'Show food regions',
            'Show cuisine options'
        ],
        'responses': [
            f'Here are some cuisine types: {cuisine}',
            f'For which cuisine would you like a recipe?: {cuisine}?'
            f'I know recipes for this cuisines: {cuisine}'
        ],
        'context_set': 'food-recipe',
    }


def food_meals_patterns():
    meals = ','.join(meal_types)
    return {
        'tag': 'food-meals',
        'patterns': [
            'What meal types we have?',
            'What meal types',
            'meal types',
            'Show meal types'
        ],
        'responses': [
            f'Here are some meal types: {meals}',
            f'For which meal types would you like a recipe: {meals}?'
            f'I know recipes for this meal types: {meals}'
        ],
        'context_set': 'food-recipe',
    }


def food_size_patterns():
    food = [
        'Are meals for one person ?',
        'Is this for one or many people?',
        'How many people is this meal for'
    ]

    return {
        'tag': 'food-size',
        'patterns': food,
        'responses': [
            'Depends from author of the recipe, ask me for recipes what cuisine, meal type or diet type you want?',
            'Each recipe has the number of people defined. Ask me for recipes what cuisine, meal type or diet type you want?'
        ],
        # 'context_set': 'food-recipe',
    }


def food_time_patterns():
    food = [
        "What's the preparation time ?",
        'How long I make the food?'
    ]
    return {
        'tag': 'food-time',
        'patterns': food,
        'responses': [
            'Depends from the dish but from 10mins to 70mins',
            'Easy recipes take about 10mins'
        ],
        'context_set': 'food-recipe',
    }


# def generate_patterns(tag):
#     return [
#         f'Do you have any {tag} food',
#         f'What shall I eat today?'
#         f'What can I eat for {meal_type}'
#         f'What is the preparation time'
#         f'Is there a ready list of products'
#         f'How many calories does the recipe have'
#         f'What tools do I need'
#         f'Are meals for one or many people'
#         f'Are there {tag} food'
#     ]


def generate_diet_patterns(tag):
    food_recipe_tag = 'food-recipe-' + tag.replace(' ', '-')

    return {
        'tag': food_recipe_tag,
        'patterns': [f'Do you have any {tag} food',
                     f'I am looking for {tag} recipes',
                     f'I am {tag} do you have any recipes ?',
                     f'{tag} food',
                     f'{tag} meal',
                     f'I want {tag}'
                     ],
        'responses': [''],
        'context_filter': 'food-recipe',
        'api': apiBaseUrl + apiRandom + '?tags=' + tag
    }


def generate_meal_type_patterns(tag):
    food_recipe_tag = 'food-recipe-' + tag.replace(' ', '-')

    return {
        'tag': food_recipe_tag,
        'patterns': [f'What can I do for {tag} ? ',
                     f"I don't know what to eat for {tag}",
                     f'{tag} food',
                     f'{tag} meal',
                     f'I want {tag}'
                     ],
        'responses': [''],
        'context_filter': 'food-recipe',
        'api': apiBaseUrl + apiRandom + '?tags=' + tag
    }


def generate_cuisine_patterns(tag):
    food_recipe_tag = 'food-recipe-' + tag.replace(' ', '-')

    return {
        'tag': food_recipe_tag,
        'patterns': [
            f"I like {tag} cuisine",
            f"I like {tag} food",
            f'{tag} food',
            f'{tag} meal',
            f'I want {tag}'
        ],
        'responses': [''],
        'context_filter': 'food-recipe',
        'api': apiBaseUrl + apiRandom + '?tags=' + tag
    }


def generate_food_recipes_objects():
    tags_objects = []
    for diet in diets:
        tags_objects.append(generate_diet_patterns(diet))
    for meal_type in meal_types:
        tags_objects.append(generate_meal_type_patterns(meal_type))
    for cuisine in cuisines:
        tags_objects.append(generate_cuisine_patterns(cuisine))

    tags_objects.append(food_like_patterns())
    tags_objects.append(food_diets_patterns())
    tags_objects.append(food_cuisine_patterns())
    tags_objects.append(food_meals_patterns())
    tags_objects.append(food_size_patterns())
    tags_objects.append(food_time_patterns())

    with open('intents_food_recipes.json', 'w', encoding="utf-8") as outfile:
        json.dump(tags_objects, outfile, indent=4)


generate_food_recipes_objects()
