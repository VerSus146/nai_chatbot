import json

f = open('intents_rawg.json')
rawg_json = json.load(f)
f.close()

f = open('intents_rawg_pl.json')
rawg_json_pl = json.load(f)
f.close()

f = open('intents_food_recipes.json')
food_json = json.load(f)
f.close()

# f = open('intents_food_pl.json')
# food_json_pl = json.load(f)
# f.close()

default_json = [
    {
        "tag": "greeting",
        "patterns": ["Hi", "Hey", "Is anyone there?", "Hello", "Good day", "Whats up"],
        "responses": ["Hello!", "Good to see you again!", "Hi there, how can I help?", "Welcome!"]
    },
    {
        "tag": "goodbye",
        "patterns": ["Cya", "See you later", "Goodbye", "Bye", "I am Leaving", "Have a Good day"],
        "responses": ["Sad to see you go :(", "Talk to you later", "Goodbye!"]
    },
    {
        "tag": "age",
        "patterns": ["How old are you?", "What is your age", "Age?"],
        "responses": ["I was developed ~3 days ago!"]
    },
    {
        "tag": "name",
        "patterns": ["What is your name", "What should I call you", "Whats your name?"],
        "responses": ["I'm NAI Chatbot!", "I don't have a name, I just talk!", "Let's not get too personal, eh?"]
    },
    {
        "tag": "help",
        "patterns": ["Can you help me?", "Help", "What can you do?", "What information do you have?",
                     "I need some help", "I need assistance", "I need help", "I need some information"],
        "responses": ["You can ask me general information about Video Games! Ask something about games,"
                      " maybe I will know the answer! Also, If you ask about specific game, you can get more"
                      " information about it! \n\n You can also ask me something about Food!"]
    },
]

default_json_pl = [
    {
        "tag": "greeting_pl",
        "patterns": ["Czesc", "Hej", "Czy ktos tam jest?", "Helol", "Dzien dobry", "Co tam?"],
        "responses": ["Czesc!", "Milo Ciebie znowu widziec!", "Hejka, jak moge pomoc?", "Witam!"]
    },
    {
        "tag": "goodbye_pl",
        "patterns": ["Narka", "Do zobaczenia", "Do widzenia", "Wychodze", "Milego dnia"],
        "responses": ["Szkoda ze juz idziesz :(", "Do uslyszenia", "Czesc!"]
    },
    {
        "tag": "age_pl",
        "patterns": ["Ile masz lat?", "Jaki jest twoj wiek?", "Wiek?"],
        "responses": ["Zostalem zakodowany ~3 dni temu!"]
    },
    {
        "tag": "name_pl",
        "patterns": ["Jak sie nazywasz?", "Jak powinienem Ciebie nazywac?", "Jak Ci na imie?"],
        "responses": ["Jestem NAI Chatbot!", "Nie mam imienia, po prostu gadam!",
                      "Moze nie bedziemy sie przyjaznic, co?"]
    },
    {
        "tag": "help_pl",
        "patterns": ["Mozesz mi pomoc?", "Pomoc", "Co mozesz zrobic?", "Jakie informacje posiadasz?",
                     "Potrzebuje troche pomocy", "Potrzebuje pomocy", "Potrzebuje troche informacji"],
        "responses": ["Mozesz sie mnie zapytac o generalne rzeczy zwiazane z grami! Zadaj pytanie na temat gier,"
                      " moze bede potrafil Ci odpowiedziec! Jezeli chcesz poznac wiecej informacji na temat gry po "
                      "prostu zadaj pytanie na jej temat!\n\n Mozesz tez zapytac o rzeczy zwiazane z jedzeniem!"]
    },
]

# rawg_json.extend(food_json)
# rawg_json.extend(food_json_pl)
rawg_json.extend(rawg_json_pl)
rawg_json.extend(default_json)
rawg_json.extend(default_json_pl)
rawg_json.extend(food_json)

with open('intents.json', 'w') as outfile:
    json.dump(rawg_json, outfile, indent=4)
