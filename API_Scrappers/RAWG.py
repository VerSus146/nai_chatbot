import json
import random
import requests
from numpy import ceil


def get_rawg_api(endpoint, params=None):
    """
    Get the data from the API
    """
    url = f"https://api.rawg.io/api/{endpoint}?key=f8251f5790d64940bbf1972dba8d4ff3&{params}"
    response = requests.get(url)
    return response.json()


def get_amount_of_games(amount):
    """
    Get the amount of games from the API

    Max page size is 40. If the amount is higher than 40, it will be divided by 40 and the remainder will be added to
    the list.
    """
    pages = ceil(amount / 40)
    list_of_games = []
    for page in range(1, int(pages + 1)):
        games = get_rawg_api(f"games",  f"page={page}&page_size=40")
        list_of_games.extend(games['results'])
    return list_of_games


def get_games_descriptions(games):
    """
    Get the description of provided games
    """
    games_descriptions = []
    for game in games:
        game_description = get_rawg_api(f"games/{game['id']}")
        games_descriptions.append([game['name'], game_description])
    return games_descriptions


def split_description_into_facts(game_description):
    """
    Split the description of the game into facts
    """
    addition = []
    addition.append(" What else would you like to know about this game?")
    addition.append(" What else are you interested in regarding this game?")

    game_facts = []
    for fact in game_description['description_raw'].split('.'):
        if fact != "":
            game_facts.append(game_description['name'] + ": " + fact.replace("\n", "").replace("\r", "")
                              .replace("\t", "") + ".")

    for i in range(len(game_facts)):
        game_facts[i] += random.choice(addition)

    return game_facts


def generate_pattern_from_game_desc(game_desc):
    """
    Generate a pattern from the game description
    """

    game_patterns = []

    game_patterns.append(game_desc['name'])
    game_patterns.append(f"What about {game_desc['name']}?")
    game_patterns.append(f"What do you know about {game_desc['name']}?")
    game_patterns.append(f"Do you know {game_desc['name']}?")
    game_patterns.append(f"Did you play {game_desc['name']}?")
    game_patterns.append(f"Have you heard about {game_desc['name']}?")
    game_patterns.append(f"Let's talk about {game_desc['name']}.")
    game_patterns.append(f"{game_desc['name']}")

    for alt_name in game_desc['alternative_names']:
        game_patterns.append(alt_name)
        game_patterns.append(f"What about {alt_name}?")
        game_patterns.append(f"What do you know about {alt_name}?")
        game_patterns.append("Do you know " + alt_name + "?")
        game_patterns.append(f"Did you play {alt_name}?")
        game_patterns.append(f"Have you heard about {alt_name}?")
        game_patterns.append(f"Let's talk about {alt_name}.")

    genere_pattern = ""
    for genere in game_desc["genres"]:
        genere_pattern += genere["name"] + " "

    for developer in game_desc['developers']:
        game_patterns.append(genere_pattern + "from " + developer["name"])

    return game_patterns


def generate_reddit_pattern_from_game_desc(game_desc):
    """
        Generate a reddit pattern from the game description
    """
    reddit_info = []

    reddit_info.append(f"{game_desc['name']} reddit info")
    reddit_info.append(f"Reddit for {game_desc['name']}")
    reddit_info.append(f"{game_desc['name']} reddit")
    reddit_info.append(f"reddit")

    for alt_name in game_desc['alternative_names']:
        reddit_info.append(f"{alt_name} reddit info")
        reddit_info.append(f"Reddit for {alt_name}")
        reddit_info.append(f"{alt_name} reddit")

    return reddit_info


def generate_company_pattern_from_game_desc(game_desc):
    """
        Generate a company pattern from the game description
    """
    company_info = []

    company_info.append(f"Who made this game?")
    company_info.append(f"Who developed this game?")
    company_info.append(f"Who is the developer of this game?")
    company_info.append(f"Who made it?")
    company_info.append(f"{game_desc['name']} company info")
    company_info.append(f"{game_desc['name']} company")
    company_info.append(f"{game_desc['name']} developers")
    company_info.append(f"{game_desc['name']} developers info")

    for alt_name in game_desc['alternative_names']:
        company_info.append(f"{alt_name} company info")
        company_info.append(f"{alt_name} company")
        company_info.append(f"{alt_name} developers")
        company_info.append(f"{alt_name} developers info")

    company_info.append(f"company")

    return company_info


def generate_developer_response_from_game_desc(game_desc):
    """
    Generate a developer response from the game description
    """

    developers = f"The developers of {game_desc['name']} are: "

    for developer in game_desc['developers']:
        developers += developer["name"] + ", "

    developers = developers[:-2] + ".\nThe publishers are: "

    for publisher in game_desc['publishers']:
        developers += publisher["name"] + ", "

    developers = developers[:-2] + "."

    return [developers]


def generate_shop_pattern_from_game_desc(game_desc):
    """
    Generate a shop pattern from the game description
    """
    shop_pattern = []

    shop_pattern.append(f"Where can I buy {game_desc['name']}?")
    shop_pattern.append(f"How can i play {game_desc['name']}?")
    shop_pattern.append(f"Which store has {game_desc['name']}?")
    shop_pattern.append(f"Which online stores have this game {game_desc['name']}?")

    for alt_name in game_desc['alternative_names']:
        shop_pattern.append(f"Where can I buy {alt_name}?")
        shop_pattern.append(f"How can i play {alt_name}?")
        shop_pattern.append(f"Which store has {alt_name}?")

    shop_pattern.append("Where can I buy this game?")
    shop_pattern.append("How can i play this game?")
    shop_pattern.append("Which store has this game?")
    shop_pattern.append("Which online stores have this game?")

    shop_pattern.append("Where can I buy it?")
    shop_pattern.append("How can i play it?")
    shop_pattern.append("Which store has it?")
    shop_pattern.append("Which online stores have it?")

    return shop_pattern


def generate_age_pattern_from_game_desc(game_desc):
    """
    Generate a age pattern from the game description
    """
    age_pattern = []

    age_pattern.append(f"Can my child play it?")
    age_pattern.append(f"For whom is this game?")
    age_pattern.append(f"Is it children friendly?")
    age_pattern.append(f"Who can play it?")
    age_pattern.append(f"Is {game_desc['name']} for children?")
    age_pattern.append(f"Is {game_desc['name']} for kids?")
    age_pattern.append(f"Is {game_desc['name']} a kids game?")
    age_pattern.append(f"Is {game_desc['name']} appropriate for kids?")

    for alt_name in game_desc['alternative_names']:
        age_pattern.append(f"Is {alt_name} for children?")
        age_pattern.append(f"Is {alt_name} for kids?")
        age_pattern.append(f"Is {alt_name} a kids game?")

    return age_pattern


def generate_generes_response_from_game_desc(generes_list):
    """
    Generate a generes response from the game description
    """
    data = ""
    for generes in generes_list:
        data += generes["name"] + ", "

    data = data[:-2] + "."

    return data


def generate_age_response_from_game_desc(game_desc):
    if game_desc["esrb_rating"] is not None:
        return [f"{game_desc['name']} is rated {game_desc['esrb_rating']['name']}."]
    else:
        return [f"{game_desc['name']} is not rated."]


def generate_genere_response_from_generes_list(games_list):
    """
    Generate a genere response from the generes list
    """
    data = ""

    for game in games_list:
        data += game + ", "

    data = data[:-2] + "."

    return data


def generate_developer_info_response_from_game_desc(games_list):
    """
    Generate a developer info response from the games list
    """
    data = ""

    for game in games_list:
        data += game + ", "

    data = data[:-2] + "."

    return data


def construct_intents():
    """
    Construct the intents from the games descriptions
    """
    get_games = get_amount_of_games(1)
    get_games_desc = get_games_descriptions(get_games)
    developers_list = []
    generes_list = []
    json_data = []
    for game_desc in get_games_desc:
        # Specific game related
        json_data.append({
            "tag": game_desc[1]['slug'],
            "patterns": generate_pattern_from_game_desc(game_desc[1]),
            "responses": split_description_into_facts(game_desc[1]),
            "context_set": game_desc[1]['slug']
        })
        json_data.append({
            "tag": "reddit_info",
            "patterns": generate_reddit_pattern_from_game_desc(game_desc[1]),
            "responses": ["Reddit for " + game_desc[1]['name'] + ": " + game_desc[1]['reddit_url']],
            "context_filter": game_desc[1]['slug'],
        })
        json_data.append({
            "tag": "developer_info",
            "patterns": generate_company_pattern_from_game_desc(game_desc[1]),
            "responses": generate_developer_response_from_game_desc(game_desc[1]),
            "context_filter": game_desc[1]['slug'],
        })
        json_data.append({
            "tag": "shop_info",
            "patterns": generate_shop_pattern_from_game_desc(game_desc[1]),
            "responses": ['Sure, let me quickly check the shops database. \n'],
            "context_filter": game_desc[1]['slug'],
            "api": f"https://api.rawg.io/api/games/{game_desc[1]['id']}/stores?key=f8251f5790d64940bbf1972dba8d4ff3"
        })
        json_data.append({
            "tag": "age_info",
            "patterns": generate_age_pattern_from_game_desc(game_desc[1]),
            "responses": generate_age_response_from_game_desc(game_desc[1]),
            "context_filter": game_desc[1]['slug']
        })

        for developer in game_desc[1]['developers']:
            if len(developer['name']) == 0:
                developers_list.append({
                    "name": developer['name'].replace(" ", "_"),
                    "games": [game_desc[1]['name']]
                })
                continue

            found = False
            for dev in developers_list:
                if dev['name'] == developer['name'].replace(" ", "_"):
                    dev['games'].append(game_desc[1]['name'])
                    found = True
                    break

            if not found:
                developers_list.append({
                    "name": developer['name'].replace(" ", "_"),
                    "games": [game_desc[1]['name']]
                })

        for game_genere in game_desc[1]['genres']:
            if len(generes_list) == 0:
                generes_list.append({
                    "name": game_genere['name'],
                    "games": [game_desc[1]['name']]
                })
                continue

            found = False
            for genere in generes_list:
                if game_genere['name'] == genere['name']:
                    genere['games'].append(game_desc[1]['name'])
                    found = True
                    break

            if not found:
                generes_list.append({
                    "name": game_genere['name'],
                    "games": [game_desc[1]['name']]
                })


    # Not specific game related
    json_data.append({
        "tag": "generes_info",
        "patterns": ['What game generes are there?', 'What are the generes?', 'What are the game generes?'],
        "responses": [f'These are the Video Game generes I know: '
                      f'{generate_generes_response_from_game_desc(generes_list)}'],
    })

    for genere in generes_list:
        json_data.append({
            "tag": f'{genere["name"]}_info',
            "patterns": [f'What games are there in {genere["name"]} genere?',
                         f'Do you know any {genere["name"]} games?', f'{genere["name"]} games'],
            "responses": [f'These are the games in {genere["name"]} '
                          f'genere: {generate_genere_response_from_generes_list(genere["games"])}'],
        })

    for developer in developers_list:
        json_data.append({
            "tag": f"{developer['name']}_info",
            "patterns": [f'What games did {developer["name"].replace("_", " ")} create?',
                         f'What did {developer["name"].replace("_", " ")} make',
                         f'What {developer["name"].replace("_", " ")} made?'],
            "responses": [f'These are the Video Games {developer["name"].replace("_", " ")} created: '
                          f'{generate_developer_info_response_from_game_desc(developer["games"])}'],
        })

    with open('intents_rawg.json', 'w', encoding="utf-8") as outfile:
        json.dump(json_data, outfile, indent=4)


construct_intents()
