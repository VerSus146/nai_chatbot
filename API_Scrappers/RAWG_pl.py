import json
import random
import requests
from numpy import ceil


def get_rawg_api(endpoint, params=None):
    """
    Get the data from the API
    """
    url = f"https://api.rawg.io/api/{endpoint}?key=f8251f5790d64940bbf1972dba8d4ff3&{params}"
    response = requests.get(url)
    return response.json()


def get_amount_of_games(amount):
    """
    Get the amount of games from the API

    Max page size is 40. If the amount is higher than 40, it will be divided by 40 and the remainder will be added to
    the list.
    """
    pages = ceil(amount / 40)
    list_of_games = []
    for page in range(1, int(pages + 1)):
        games = get_rawg_api(f"games",  f"page={page}&page_size=40")
        list_of_games.extend(games['results'])
    return list_of_games


def get_games_descriptions(games):
    """
    Get the description of provided games
    """
    games_descriptions = []
    for game in games:
        game_description = get_rawg_api(f"games/{game['id']}")
        games_descriptions.append([game['name'], game_description])
    return games_descriptions


def split_description_into_facts(game_description):
    """
    Split the description of the game into facts
    """
    addition = []
    addition.append(" Co jeszcze chcialbys wiedziec na temat tej gry?")
    addition.append(" Jestes jeszcze zainteresowany czyms innym na temat tej gry?")

    game_facts = []
    for fact in game_description['description_raw'].split('.'):
        if fact != "":
            game_facts.append(game_description['name'] + ": " + fact.replace("\n", "").replace("\r", "")
                              .replace("\t", "") + ".")

    for i in range(len(game_facts)):
        game_facts[i] += random.choice(addition)

    return game_facts


def generate_pattern_from_game_desc(game_desc):
    """
    Generate a pattern from the game description
    """

    game_patterns = []

    game_patterns.append(game_desc['name'])
    game_patterns.append(f"Co na temat {game_desc['name']}?")
    game_patterns.append(f"Co wiesz o {game_desc['name']}?")
    game_patterns.append(f"Znasz {game_desc['name']}?")
    game_patterns.append(f"Grales w {game_desc['name']}?")
    game_patterns.append(f"Slyszales o {game_desc['name']}?")
    game_patterns.append(f"Porozmawiajmy o {game_desc['name']}.")
    game_patterns.append(f"{game_desc['name']}")

    for alt_name in game_desc['alternative_names']:
        game_patterns.append(alt_name)
        game_patterns.append(f"Co na temat {alt_name}?")
        game_patterns.append(f"Co wiesz o {alt_name}?")
        game_patterns.append(f"Znasz {alt_name}?")
        game_patterns.append(f"Grales w {alt_name}?")
        game_patterns.append(f"Slyszales o {alt_name}?")
        game_patterns.append(f"Porozmawiajmy o {alt_name}.")

    genere_pattern = ""
    for genere in game_desc["genres"]:
        genere_pattern += genere["name"] + " "

    for developer in game_desc['developers']:
        game_patterns.append(genere_pattern + "od " + developer["name"])

    return game_patterns


def generate_reddit_pattern_from_game_desc(game_desc):
    """
        Generate a reddit pattern from the game description
    """
    reddit_info = []

    reddit_info.append(f"{game_desc['name']} reddit link")
    reddit_info.append(f"Reddit dla {game_desc['name']}")
    reddit_info.append(f"{game_desc['name']} reddit")
    reddit_info.append(f"reddit")

    for alt_name in game_desc['alternative_names']:
        reddit_info.append(f"{alt_name} reddit link")
        reddit_info.append(f"Reddit dla {alt_name}")
        reddit_info.append(f"{alt_name} reddit")

    return reddit_info


def generate_company_pattern_from_game_desc(game_desc):
    """
        Generate a company pattern from the game description
    """
    company_info = []

    company_info.append(f"Kto stworzyl ta gre?")
    company_info.append(f"Kto jest deweloperem tej gry?")
    company_info.append(f"Kto jest tworca tej gry?")
    company_info.append(f"Kto napisal tą gre?")
    company_info.append(f"{game_desc['name']} firma informacje")
    company_info.append(f"{game_desc['name']} firma")
    company_info.append(f"{game_desc['name']} deweloper")
    company_info.append(f"{game_desc['name']} deweloper informacje")

    for alt_name in game_desc['alternative_names']:
        company_info.append(f"{alt_name} firma informacje")
        company_info.append(f"{alt_name} firma")
        company_info.append(f"{alt_name} deweloper")
        company_info.append(f"{alt_name} deweloper informacje")

    company_info.append(f"firma")

    return company_info


def generate_developer_response_from_game_desc(game_desc):
    """
    Generate a developer response from the game description
    """

    developers = f"Deweloperami {game_desc['name']} są: "

    for developer in game_desc['developers']:
        developers += developer["name"] + ", "

    developers = developers[:-2] + ".\nWydawcami są: "

    for publisher in game_desc['publishers']:
        developers += publisher["name"] + ", "

    developers = developers[:-2] + "."

    return [developers]


def generate_shop_pattern_from_game_desc(game_desc):
    """
    Generate a shop pattern from the game description
    """
    shop_pattern = []

    shop_pattern.append(f"Gdzie moge kupic {game_desc['name']}?")
    shop_pattern.append(f"Jak moge zagrac w {game_desc['name']}?")
    shop_pattern.append(f"Ktory sklep ma {game_desc['name']}?")
    shop_pattern.append(f"Ktore sklepy online maja gre {game_desc['name']}?")

    for alt_name in game_desc['alternative_names']:
        shop_pattern.append(f"Gdzie moge kupic {alt_name}?")
        shop_pattern.append(f"Jak moge zagrac w {alt_name}?")
        shop_pattern.append(f"Ktory sklep ma {alt_name}?")

    shop_pattern.append("Gdzie moge kupic ta gre?")
    shop_pattern.append("Jak moge zagrac w ta gre?")
    shop_pattern.append("Ktory sklep ma ta gre?")
    shop_pattern.append("Ktore sklepy online maja ta gre?")

    shop_pattern.append("Gdzie moge to kupic?")
    shop_pattern.append("Jak moge w to zagrac?")
    shop_pattern.append("Ktory sklep ma to?")
    shop_pattern.append("Ktore sklepy online maja to?")

    return shop_pattern


def generate_age_pattern_from_game_desc(game_desc):
    """
    Generate a age pattern from the game description
    """
    age_pattern = []

    age_pattern.append(f"Czy moje dziecko moze w to grac?")
    age_pattern.append(f"Dla kogo jest ta gra?")
    age_pattern.append(f"Czy jest to gra dla dzieci?")
    age_pattern.append(f"Kto moze w to grac?")
    age_pattern.append(f"Czy {game_desc['name']} jest dla dzieci?")
    age_pattern.append(f"{game_desc['name']} jest dla dzieci?")
    age_pattern.append(f"Czy gra {game_desc['name']} jest dla dzieci?")
    age_pattern.append(f"Czy {game_desc['name']} jest odpowiednia dla dzieci?")

    for alt_name in game_desc['alternative_names']:
        age_pattern.append(f"Czy {alt_name} jest dla dzieci?")
        age_pattern.append(f"{alt_name} jest dla dzieci?")
        age_pattern.append(f"Czy {alt_name} jest odpowiednia dla dzieci?")

    return age_pattern


def generate_generes_response_from_game_desc(generes_list):
    """
    Generate a generes response from the game description
    """
    data = ""
    for generes in generes_list:
        data += generes["name"] + ", "

    data = data[:-2] + "."

    return data


def generate_age_response_from_game_desc(game_desc):
    if game_desc["esrb_rating"] is not "":
        return [f"Gra {game_desc['name']} oceniona jest jako {game_desc['esrb_rating']['name']}."]
    else:
        return [f"Gra {game_desc['name']} nie jest oceniona."]


def generate_genere_response_from_generes_list(games_list):
    """
    Generate a genere response from the generes list
    """
    data = ""

    for game in games_list:
        data += game + ", "

    data = data[:-2] + "."

    return data


def generate_developer_info_response_from_game_desc(games_list):
    """
    Generate a developer info response from the games list
    """
    data = ""

    for game in games_list:
        data += game + ", "

    data = data[:-2] + "."

    return data


def construct_intents():
    """
    Construct the intents from the games descriptions
    """
    get_games = get_amount_of_games(1)
    get_games_desc = get_games_descriptions(get_games)
    developers_list = []
    generes_list = []
    json_data = []
    for game_desc in get_games_desc:
        # Specific game related
        json_data.append({
            "tag": f"{game_desc[1]['slug']}_pl",
            "patterns": generate_pattern_from_game_desc(game_desc[1]),
            "responses": split_description_into_facts(game_desc[1]),
            "context_set": game_desc[1]['slug']
        })
        json_data.append({
            "tag": "reddit_info_pl",
            "patterns": generate_reddit_pattern_from_game_desc(game_desc[1]),
            "responses": ["Reddit for " + game_desc[1]['name'] + ": " + game_desc[1]['reddit_url']],
            "context_filter": game_desc[1]['slug'],
        })
        json_data.append({
            "tag": "developer_info_pl",
            "patterns": generate_company_pattern_from_game_desc(game_desc[1]),
            "responses": generate_developer_response_from_game_desc(game_desc[1]),
            "context_filter": game_desc[1]['slug'],
        })
        json_data.append({
            "tag": "shop_info_pl",
            "patterns": generate_shop_pattern_from_game_desc(game_desc[1]),
            "responses": ['Okay, pozwol ze sprawdze w mojej bazie danych. \n'],
            "context_filter": game_desc[1]['slug'],
            "api": f"https://api.rawg.io/api/games/{game_desc[1]['id']}/stores?key=f8251f5790d64940bbf1972dba8d4ff3"
        })
        json_data.append({
            "tag": "age_info_pl",
            "patterns": generate_age_pattern_from_game_desc(game_desc[1]),
            "responses": generate_age_response_from_game_desc(game_desc[1]),
            "context_filter": game_desc[1]['slug']
        })

        for developer in game_desc[1]['developers']:
            if len(developer['name']) == 0:
                developers_list.append({
                    "name": developer['name'].replace(" ", "_"),
                    "games": [game_desc[1]['name']]
                })
                continue

            found = False
            for dev in developers_list:
                if dev['name'] == developer['name'].replace(" ", "_"):
                    dev['games'].append(game_desc[1]['name'])
                    found = True
                    break

            if not found:
                developers_list.append({
                    "name": developer['name'].replace(" ", "_"),
                    "games": [game_desc[1]['name']]
                })

        for game_genere in game_desc[1]['genres']:
            if len(generes_list) == 0:
                generes_list.append({
                    "name": game_genere['name'],
                    "games": [game_desc[1]['name']]
                })
                continue

            found = False
            for genere in generes_list:
                if game_genere['name'] == genere['name']:
                    genere['games'].append(game_desc[1]['name'])
                    found = True
                    break

            if not found:
                generes_list.append({
                    "name": game_genere['name'],
                    "games": [game_desc[1]['name']]
                })


    # Not specific game related
    json_data.append({
        "tag": "generes_info_pl",
        "patterns": ['Jakie kategorie znasz?', 'Jakie sa kategorie gier?', 'Jakie gatunki znasz?'],
        "responses": [f'Znam takie gatunki gier: '
                      f'{generate_generes_response_from_game_desc(generes_list)}'],
    })

    for genere in generes_list:
        json_data.append({
            "tag": f'{genere["name"]}_info_pl',
            "patterns": [f'Jakie gry sa w gatunku {genere["name"]}?',
                         f'Znasz jakies gry {genere["name"]}?', f'{genere["name"]} gry'],
            "responses": [f'To sa gry o gatunku {genere["name"]} '
                          f': {generate_genere_response_from_generes_list(genere["games"])}'],
        })

    for developer in developers_list:
        json_data.append({
            "tag": f"{developer['name']}_info_pl",
            "patterns": [f'Jakie gry stworzyli {developer["name"].replace("_", " ")}?',
                         f'Co {developer["name"].replace("_", " ")} zrobili',
                         f'Co {developer["name"].replace("_", " ")} stworzyli?'],
            "responses": [f'Te gry stworzyla firma {developer["name"].replace("_", " ")}: '
                          f'{generate_developer_info_response_from_game_desc(developer["games"])}'],
        })

    with open('intents_rawg_pl.json', 'w') as outfile:
        json.dump(json_data, outfile, indent=4)


construct_intents()
