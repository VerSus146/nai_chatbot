''' NLP Chatbot Deep Learning by Tensorflow and NLTK '''
import nltk  # natural language tool kits
import numpy
import requests
import tensorflow as tf
import json
import os

'''
Autorzy: Krystian Dąbrowski s18550, Krzysztof Windorpski s18562

Projekt: NLP ChatBot

Problem:
1. Stworzyć chatbota który będzie rozmawiał z nami o naszych zainteresowaniach: gry i jedzenie 

Podsumowanie:

Uczenie modelu:
Intent ( "zamiar" uzytkownika ) obiekty ktore maja tag, pattern,response,context,api
- tokenizujemy i stemmingujemy(rozdzielamy ciagi zdaniowe na male czescie) LancasterStemm dane "intentsy" i uczymy nimi sieć neuronową

Stworzyliśmy dane do uczenia na które składają się:
- tags - który jest oznaczeniem 
- patterns - wzroce zdaniowe jakie może wpisać użytkownik a nasz robot rozpozna, wymagane jest 80% matchingu
- responses - zbiór odpowiedzi które może odpisać robot - robot wybiera odpowiedzi randomowo
- context - dzięki ustawianiu contextu robotowi jest łatwiej odpowiadać na kolejne pytania ponieważ może przeszukiwać tylko dane z konkretnego contextu
- api - niektore intensty odpytuja endpoint api i dynamicznie zwracaja nam dane

Przyjmowanie danych uzytkownika
- W czasie rzeczywistym tokenizujemy i stemmingujemy Lancasterem to co wpisze uzytkownik
- Probujemy dopasowac user input z jednym z intentsów i otrzymujemy tag Intentu
- Jezeli sie powiedzie i intent posiada context_set, jesteśmy w stanie ustalić kontekst konwersacji co przyspieszy i ukierunkuje wyszukiwanie odpowiedzi
- Gdy robot nie bedzie w stanie znalezc Intentu dla danego kontekstu, zaczyna szukac we wszystkich intentach i ewnetualnie ustawia nowy kontekst rozmowy

Przebudowanie modelu, żeby załadować więcej intents
- usun katalog model.chatbot
- uruchom spoonacular.py
- uruchom RAWG.py
- uruchom RAWG_pl.py
- uruchom intents_builder.py
- finalnie uruchom main.py - jezeli skrypt nie znajdzie modelu to na 100% wygeneruje nowy

Uruchomienie
- uruchom main.py

'''


def load_data(json_file='API_Scrappers/intents.json'):
    '''
    load intents file
    intents => { tag, patterns, responses, context_set, context_filter, api }
    '''
    with open(json_file) as f:
        dataset = json.load(f)

    return dataset


def tokenize_words(dataset):
    '''
    tokenize words from dataset
    '''
    words = []
    intents_x = []
    # Get intents from dataset

    for intent in dataset:
        try:
        # Create tokens from patterns and responses
            for p in intent['patterns'] + intent['responses']:
                tokens = nltk.regexp_tokenize(p, "[\w']+")
                words.extend(tokens)
                intents_x.append(tokens)
        except TypeError:
            print(intent)


    intent_tags = []
    intents_y = []
    # Get all tags insert them in lists
    for intent in dataset:
        for p in intent['patterns'] + intent['responses']:
            intents_y.append(intent["tag"])
        if intent['tag'] not in intent_tags:
            intent_tags.append(intent['tag'])

    # Sort tags
    intent_tags = sorted(intent_tags)

    return (words, intents_x, intents_y, intent_tags)


def stemming(words):
    '''
    A word stemmer based on the Lancaster algorithm.
    For finding the base of words.
    '''

    stemmer = nltk.stem.LancasterStemmer()
    words_tmp = []
    for w in words:
        # Add words to temporary list
        words_tmp.append(stemmer.stem(w.lower()))

    # Remove duplicates and add to list
    root_words = sorted(set(words_tmp))

    return root_words


def get_train_data(root_words, intents_x, intents_y, intent_tags):
    '''
    Create training data for the model from provided params
    '''
    stemmer = nltk.stem.LancasterStemmer()

    training = []
    output = []

    # Create training data
    for i in range(len(intents_x)):
        bag_of_words = []
        words_tmp = []
        for w in intents_x[i]:
            # Append stemmed words to list
            words_tmp.append(stemmer.stem(w.lower()))

        # Create bag of words
        for w in root_words:
            found = 1 if w in words_tmp else 0
            bag_of_words.append(found)

        training.append(bag_of_words)

        # Create output data for model
        output_row = [0] * len(intent_tags)
        output_row[intent_tags.index(intents_y[i])] = 1
        output.append(output_row)

    train_x = numpy.array(training)
    train_y = numpy.array(output)

    return (train_x, train_y)


def build_model(train_x, train_y):
    '''
    Build a model from training data
    '''


    epochs = 1000
    batch_size = 16

    # Create all connected Dense layers for model training to create a network of word connections
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(batch_size, input_shape=(len(train_x[0]),)))
    model.add(tf.keras.layers.Dense(batch_size))
    model.add(tf.keras.layers.Dense(batch_size))
    model.add(tf.keras.layers.Dense(len(train_y[0]), activation='softmax'))

    # Compile model https://machinelearningmastery.com/cross-entropy-for-machine-learning/
    model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])

    # Train model
    model.fit(train_x, train_y, epochs=epochs, batch_size=batch_size, verbose=0)

    # Evaluate model accuracy
    model.summary()  # for debug
    test_loss, test_acc = model.evaluate(train_x, train_y, verbose=0)
    print("\nTested Loss:{} Acc:{}".format(test_loss, test_acc))

    return model


def prepare_model(dataset, model_file="model.chatbot"):
    '''
    Prepare model for use
    '''

    # Tokenize
    words, intents_x, intents_y, intent_tags = tokenize_words(dataset)

    # Stemming
    root_words = stemming(words)

    # Check if Model file exists
    if os.path.isdir(model_file) or os.path.isfile(model_file):
        model = tf.keras.models.load_model(model_file)
        print("Loading model successfully")
    else:
        # Train new model
        train_x, train_y = get_train_data(root_words, intents_x, intents_y, intent_tags)
        model = build_model(train_x, train_y)
        model.save(model_file)

    return (model, root_words, intent_tags)


def chat(model, root_words, intent_tags):
    '''
    Chat with the bot
    '''
    print("Start talking with the bot (type quit to stop)")
    # Set context of the talk
    context_state = None
    while True:
        user_input = input("You: ")
        if user_input.lower() == "quit":
            break

        # Tokenize the input
        tokens = nltk.regexp_tokenize(user_input, "[\w']+")

        # Stemm the input
        stemmer = nltk.stem.LancasterStemmer()
        tokens_tmp = []
        for t in tokens:
            tokens_tmp.append(stemmer.stem(t.lower()))

        # Create a bag of words from the input depending on the root words
        bag_of_words = []
        for w in root_words:
            if w in tokens_tmp:
                found = 1
            else:
                found = 0
            bag_of_words.append(found)

        # Predict the intent of the input
        probilities = model.predict(numpy.array([bag_of_words]))
        max_index = numpy.argmax(probilities)
        max_probility = probilities[0][max_index]

        if max_probility < 0.75:
            print("Sorry, I don't know what you mean. "
                  "I probably didn't yet learn this. :( (confidence: {})".format(max_probility))
            continue
        else:
            # Get the intent tag
            found_tag = intent_tags[max_index]

        context_state = match_response(found_tag, context_state)


def api_calls(endpoint) -> str:
    '''
    Make an API call to the endpoint
    '''

    data = ""
    if 'rawg.io' in endpoint:
        response = requests.get(endpoint)
        if 'stores' in endpoint:
            resp = response.json()
            for store in resp['results']:
                data += store['url'] + '\n'
    elif 'spoonacular' in endpoint:
        response = requests.get(endpoint,{'apiKey': '1afe6f6ef6b449908cda8809bf889d67'})
        return 'Here try this recipe:' + response.json()['recipes'][0]['spoonacularSourceUrl']

    return data


def match_response(found_tag, context_state=None):
    '''
    Chose a response based on the tag, context, and api
    '''

    # For every intent in dataset
    for intent in dataset:
        # Check if the intent tag matches the found tag
        if intent['tag'] == found_tag:
            # Check if there's a context to the talk
            if context_state is not None:
                # Check if found intent has a context filter
                if 'context_filter' in intent:
                    # Check if the context filter matches the context state
                    if context_state in intent['context_filter']:

                        # Get all of the possible responses from this intent
                        possible_responses = intent['responses']
                        # Select a random message from the intent responses
                        print(numpy.random.choice(possible_responses))

                        # Set the context of the talk
                        if 'context_set' in intent:
                            context_state = intent['context_set']

                        # Get API call information if there is any
                        if 'api' in intent:
                            # Print API Call information
                            print(api_calls(intent['api']))

                        # Return the context state
                        return context_state
            else:
                # Get all of the possible responses from this intent
                possible_responses = intent['responses']

                # If this intent is associated with a context set, then set the context state
                if 'context_set' in intent:
                    context_state = intent['context_set']
                else:
                    context_state = None

                # Get API call information if there is any
                if 'api' in intent:
                    # Print API Call information
                    print(api_calls(intent['api']))

                # Select a random message from the intent responses
                print(numpy.random.choice(possible_responses))
                return context_state

    # Print a did not understand message
    print("thinking...")

    # Retry the match without the context state
    return match_response(found_tag)


dataset = load_data()
model, root_words, intent_tags = prepare_model(dataset)
chat(model, root_words, intent_tags)
